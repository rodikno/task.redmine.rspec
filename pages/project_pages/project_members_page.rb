require './spec/spec_helper'

class ProjectMembersPage < BasicPage
  include PageObject

  page_url BASIC_URL + '/projects/<%=params[:project_name]%>/settings/members'

  link(:add_new_member, :css => 'div#tab-content-members a.icon-add')
  div(:new_member_dialog, :css => 'div.ui-dialog')
  text_field(:user_search_field, :id => 'principal_search')
  checkbox(:set_role_manager, :css => "div.roles-selection input[value='3']")
  checkbox(:set_role_developer, :css => "div.roles-selection input[value='4']")
  checkbox(:set_role_reporter, :css => "div.roles-selection input[value='5']")
  button(:add_member_button, :id => 'member-add-submit')
  table(:members_list, :css => 'table.list.members')

  def add_member(user)
    # click on 'Add member' link and wait until search dialog appears
    self.add_new_member
    wait_until(5, "Timeout waiting for [ADD MEMBER DIALOG] to appear") do
      new_member_dialog_element.visible?
    end
    # search for a user and wait until user is found
    self.user_search_field = user.full_name
    member_to_add_elem = nil
    wait_until(5, "Timeout when waiting for desired user to appear after search") do
      member_to_add_elem = @browser.find_element(xpath: "//label[text()[contains(.,'#{user.full_name}')]]/input")
      member_to_add_elem.displayed?
    end
    # add user as a member with corresponding role
    member_to_add_elem.click
    check_set_role_manager
    add_member_button
  end

  def has_row_with_a_text?(text)
    begin
      row = self.members_list_element[text]
      row.visible?
    rescue NoMethodError
      return false
    end
  end

end