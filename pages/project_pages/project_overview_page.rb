require './spec/spec_helper'

class ProjectOverviewPage < BasicPage
  include PageObject

  page_url BASIC_URL + '/projects/<%=params[:project_name]%>'

  link(:close_project_link, :css => 'a.icon-lock')
  paragraph(:project_is_closed_warning, :css => 'p.warning')

  def accept_close_project_alert
    @browser.switch_to.alert.accept
  end

  def close_project
    self.close_project_link
    accept_close_project_alert
  end

end