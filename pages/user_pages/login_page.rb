require './spec/spec_helper'

class LoginPage < BasicPage
  include PageObject

  page_url BASIC_URL + '/login'

  text_field(:login, :id => 'username')
  text_field(:password, :id => 'password')
  button(:login_button, :name => 'login')


  def log_in_as(user)
    self.login = user.login
    self.password = user.password
    login_button
  end

end