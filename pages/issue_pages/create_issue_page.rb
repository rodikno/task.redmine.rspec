require './spec/spec_helper'

class CreateIssuePage < BasicPage
  include PageObject

  page_url BASIC_URL + '/projects/<%=params[:project_name]%>/issues/new'

  select_list(:issue_type, :css => 'select#issue_tracker_id')
  select_list(:assignee, :id => 'issue_assigned_to_id')
  text_field(:subject, :id => 'issue_subject')
  text_area(:description, :id => 'issue_description')
  button(:submit, :name => 'commit')

  def create_issue(issue)
    self.issue_type = 'Bug'
    self.subject = issue.subject
    self.description = issue.description
    submit
  end

end