require './spec/spec_helper'

class LogIssueTimePage < BasicPage
  include PageObject

  page_url BASIC_URL + '/issues/<%=params[:issue_id]%>/time_entries/new'

  text_field(:hours, :id => 'time_entry_hours')
  text_field(:comment, :id => 'time_entry_comments')
  select_list(:activity_type, :id => 'time_entry_activity_id')
  button(:create, :name => 'commit')

  def log_time(time)
    self.hours = time
    self.comment = "Many useful things done"
    self.activity_type = 'Development'
    create
  end

end