require 'faker'

class RedmineIssue

  attr_reader :subject, :description
  attr_accessor :assigned_user, :id

  def initialize(parameters={})
    @subject = parameters[:subject]             || Faker::Hacker.adjective.capitalize + Faker::Hacker.noun.capitalize
    @description = parameters[:description]     || Faker::Hacker.say_something_smart
    @assigned_user = parameters[:assigned_user] || nil
    @id = parameters[:id]                       || nil
  end

end