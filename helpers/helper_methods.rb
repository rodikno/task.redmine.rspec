require './spec/spec_helper'
require_relative 'redmine_user'
require_relative 'redmine_project'
require_relative 'redmine_issue'

module HelperMethods

  def log_out
    begin
      @browser.find_element(css: '.logout').click
    rescue Selenium::WebDriver::Error::NoSuchElementError

    end
  end

  def log_in_as(user)
    visit(LoginPage).log_in_as(user)
  end

end