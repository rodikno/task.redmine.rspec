@project
Feature: Redmine project actions

  Scenario: Create a project
    Given I register a user
    When I create a project
    Then New project is created

  Scenario: Add a member to the project
    Given I register two users: 'first user' and 'second user'
    When I log in as a 'first user'
    And I create a project
    And I add a 'second user' as a member to my project
    Then second user is added as a member to the project