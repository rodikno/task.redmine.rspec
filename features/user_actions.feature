@user
Feature: Redmine: user actions

  Background:
    Given I register a user

  Scenario: [Registration]
    Then New user is registered

  Scenario: [Log in]
    When I log out
    And I log in as a 'first user'
    Then I am logged in as a 'first user'

  Scenario: [Change password]
    When I change password to a random one
    Then My password is changed