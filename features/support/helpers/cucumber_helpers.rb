module CucumberHelpers

  def log_out
    begin
      @browser.find_element(css: '.logout').click
    rescue Selenium::WebDriver::Error::NoSuchElementError

    end
  end

  def log_in_as(user)
    log_out
    visit(LoginPage).log_in_as(user)
  end

end