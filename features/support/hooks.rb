require 'selenium-webdriver'
require_relative '../../helpers/redmine_user'
require_relative '../../helpers/redmine_project'
require_relative '../../helpers/redmine_issue'

Before do
  @browser = Selenium::WebDriver.for :chrome, :switches => %w[--ignore-certificate-errors --disable-popup-blocking --disable-translate --disable-extensions]
  @wait = Selenium::WebDriver::Wait.new(:timeout => 10)

  @first_user = RedmineUser.new
  @second_user = RedmineUser.new
  @project = RedmineProject.new
  @issue = RedmineIssue.new
end

After do
  @browser.quit if @browser
end