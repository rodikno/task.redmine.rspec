require_relative '../support/helpers/cucumber_helpers'
require 'page-object/page_factory'

World(PageObject::PageFactory, CucumberHelpers)
