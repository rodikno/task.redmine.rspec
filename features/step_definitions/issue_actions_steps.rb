And(/^I create an issue$/) do
  visit CreateIssuePage, :using_params => {:project_name => @project.name} do |page|
    page.create_issue(@issue)
    @issue.id = on(IssueDetailsPage).get_current_issue_id
  end
end

Then(/^Issue is created$/) do
  expect(on(IssueDetailsPage).success_message_element).to be_visible
end

And(/^I assign the '(.+)' to this issue$/) do |user|
  usr = nil
  case user
    when 'first user'
      usr = @first_user
    when 'second user'
      usr = @second_user
    else
      raise StandardError '[RAISED] User for issue assignment is not specified'
  end
  visit(IssueDetailsPage, :using_params => {:issue_id => @issue.id}).assign_user(usr)
  @issue.assigned_user = usr
end

Then(/^'(.+)' is assigned to an issue$/) do |user|
  usr = nil
  case user
    when 'first user'
      usr = @first_user
    when 'second user'
      usr = @second_user
    else
      raise StandardError '[RAISED] User for issue assignment is not specified'
  end
  expect(on(IssueDetailsPage).get_assigned_user_full_name).to eql(usr.full_name)
end

And(/^'(.+)' has assigned an issue to '(.+)'$/) do |user1, user2|
  steps %Q{
    When I log in as a '#{user1}'
    And I create a project
    And I add a '#{user2}' as a member to my project
    And I create an issue
    And I assign the '#{user2}' to this issue
  }
end

And(/^I log (\d+) hours for an issue$/) do |time|
  visit LogIssueTimePage, :using_params => {:issue_id => @issue.id} do |page|
    page.log_time(time.to_i)
  end
  visit(IssueDetailsPage, :using_params => {:issue_id => @issue.id})
end

Then(/^I see (\d+) hours logged for an issue$/) do |time|
  expect(on(IssueDetailsPage).get_spent_time).to eql time.to_i
end

And(/^'(.+)' has logged (\d+) hours for an issue$/) do |user, time|
  steps %Q{
    And I log in as a '#{user}'
    And I log #{time} hours for an issue
  }
end

And(/^'(.+)' closes the issue$/) do |user|
  visit(IssueDetailsPage, :using_params => {:issue_id => @issue.id}).close_issue
end

Then(/^Issue is closed$/) do
  expect(on(IssueDetailsPage).is_closed?).to be true
end

And(/^'(.+)' has logged (\d+) hours for an issue and closed it$/) do |user, time|
  steps %Q{
    And '#{user}' has logged #{time} hours for an issue
    And '#{user}' closes the issue
  }
end