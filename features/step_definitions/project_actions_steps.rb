When(/^I create a project$/) do
  visit(CreateProjectPage).create_project(@project)
end

Then(/^New project is created$/) do
  expect(on(ProjectSettingsPage)).to have_success_message
  expect(on(ProjectSettingsPage).project_title).to eql @project.name
end

And(/^I add a '(.+)' as a member to my project$/) do |user|
  usr = nil
  case user
    when 'first user'
      usr = @first_user
    when 'second user'
      usr = @second_user
    else
      raise StandardError '[RAISED] Incorrect user is specified for adding to the project'
  end
  visit ProjectMembersPage, :using_params => {:project_name => @project.name} do |page|
    page.add_member(usr)
  end
end

Then(/^second user is added as a member to the project$/) do
  @current_page.refresh
  expect(@current_page.has_row_with_a_text?(@second_user.full_name)).to be true
end

And(/^I close the project$/) do
  visit ProjectOverviewPage, :using_params => {:project_name => @project.name} do |page|
    page.close_project
    page.accept_close_project_alert
  end
end

Then(/^Project is closed$/) do
  expect(on(ProjectOverviewPage).project_is_closed_warning_element).to be_visible
end