require_relative '../../pages/user_pages/registration_page'

Given(/^I register a user$/) do
  visit(RegistrationPage).register_user(@first_user)
end

Then(/^New user is registered$/) do
  expect(on(MyAccountPage).success_message_element).to be_visible
end

When(/^I log out$/) do
  log_out
end

And(/^I log in as a '(.+)'$/) do |user|
  log_out
  case user
    when 'first user'
      log_in_as @first_user
    when 'second user'
      log_in_as @second_user
    else
      raise StandardError, '[RAISED] Please provide credentials for log in action'
  end
end

Then(/^I am logged in as a '(.+)'$/) do |user|
  user_id = nil
  case user
    when 'first user'
      user_id = @first_user.id
    when 'second user'
      user_id = @second_user.id
    else
      raise StandardError, '[RAISED] User for verification is not specified'
  end
  expect(on(BasicPage).get_current_user_id).to eql user_id
end

When(/^I change password to a random one$/) do
  new_password = 'Password!@#' + rand(1000).to_s
  visit(ChangePasswordPage).change_password(@first_user, new_password)
end

Then(/^My password is changed$/) do
  expect(on(ChangePasswordPage).success_message_element).to be_visible
end

Given(/^I register two users: 'first user' and 'second user'$/) do
  visit(RegistrationPage).register_user(@first_user)
  log_out
  visit(RegistrationPage).register_user(@second_user)
end