@issue
Feature: Redmine issue actions

  Scenario: Create issue
    When I register a user
    And I create a project
    And I create an issue
    Then Issue is created

  Scenario: Assign another user to an issue
    When I register two users: 'first user' and 'second user'
    And I log in as a 'first user'
    And I create a project
    And I add a 'second user' as a member to my project
    And I create an issue
    And I assign the 'second user' to this issue
    Then 'second user' is assigned to an issue

  Scenario: Log time for an issue
    When I register two users: 'first user' and 'second user'
    And 'first user' has assigned an issue to 'second user'
    And I log in as a 'second user'
    And I log 3 hours for an issue
    Then I see 3 hours logged for an issue

  Scenario: Close issue after time is logged for it
    When I register two users: 'first user' and 'second user'
    And 'first user' has assigned an issue to 'second user'
    And 'second user' has logged 3 hours for an issue
    And 'second user' closes the issue
    Then Issue is closed

  Scenario: Close a project with an issue I've logged time on
    When I register two users: 'first user' and 'second user'
    And 'first user' has assigned an issue to 'second user'
    And 'second user' has logged 3 hours for an issue and closed it
    And I log in as a 'first user'
    And I close the project
    Then Project is closed

