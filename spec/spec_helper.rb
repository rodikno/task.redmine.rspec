require 'rspec'
require 'require_all'
require 'page-object'

require_all './pages/**/*.rb'
require_all './spec/shared/**/*.rb'
require_all './helpers/**/*.rb'

RSpec.configure do |config|
  config.include PageObject::PageFactory
  config.include HelperMethods
  config.before :all do
    @browser = Selenium::WebDriver.for :chrome, :switches => %w[--ignore-certificate-errors --disable-popup-blocking --disable-translate --disable-extensions]
    @wait = Selenium::WebDriver::Wait.new(:timeout => 10)
  end

  config.after :all do
    @browser.quit
  end
end