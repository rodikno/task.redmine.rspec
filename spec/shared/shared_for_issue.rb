shared_context 'Create issue in a project' do |issue, project|
  before :all do
    visit CreateIssuePage, :using_params => {:project_name => project.name} do |page|
      page.create_issue(issue)
      issue.id = on(IssueDetailsPage).get_current_issue_id
    end
  end
end

shared_context 'Assign user to an issue' do |user, issue|
  before :all do
    visit(IssueDetailsPage, :using_params => {:issue_id => issue.id}).assign_user(user)
    issue.assigned_user = user
  end
end

shared_context 'User has a project with an issue assigned to another user' do |user1, project, issue, user2|
  include_context 'Register users', user1, user2
  include_context 'Log in as', user1
  include_context 'Create a new project', project
  include_context 'Add user as a member to project', user2, project
  include_context 'Create issue in a project', issue, project
  include_context 'Assign user to an issue', user2, issue
end

shared_context 'Log time for issue' do |time, issue|
  before :all do
    visit LogIssueTimePage, :using_params => {:issue_id => issue.id} do |page|
      page.log_time(time)
    end
    visit(IssueDetailsPage, :using_params => {:issue_id => issue.id})
  end
end

shared_context 'Close issue' do |issue|
  before :all do
    visit(IssueDetailsPage, :using_params => {:issue_id => issue.id}).close_issue
  end
end