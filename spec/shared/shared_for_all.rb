require './spec/spec_helper'

shared_examples 'success message is shown' do
  it ': Then success message is shown' do
    on @current_page do |page|
      expect(page).to have_success_message
    end
  end
end