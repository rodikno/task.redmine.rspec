require './spec/spec_helper'

shared_examples 'Successfully log in as' do |user|
  include_context 'Log in as', user
  it ': Then I am logged in' do
    expect(@current_page.current_url).to eql BasicPage::BASIC_URL + '/my/page'
  end
end

shared_context 'Log in as' do |user|
  before :all do
    log_out
    visit(LoginPage).log_in_as(user)
  end
end