require './spec/spec_helper'

describe '[Project: Create]', :project do
  context 'When I register a new user' do
    user = RedmineUser.new
    include_context 'Register a new user', user
    project = RedmineProject.new
    context 'When I create a new project' do
      include_context 'Create a new project', project
      it ': Then project is created' do
        on ProjectSettingsPage do |page|
          expect(page).to have_success_message
        end
      end
      it ': Then correct project title is shown' do
        on ProjectSettingsPage do |page|
          expect(page.project_title).to eql project.name
        end
      end
    end
  end
end

describe '[Project: Add member]', :project do
  context 'When I have two existing users' do
    user1 = RedmineUser.new
    user2 = RedmineUser.new
    project = RedmineProject.new
    include_context 'Register users', user1, user2
    context 'When one user adds another user to his project' do
      include_context 'Log in as', user1
      include_context 'Create a new project', project
      before :all do
        visit ProjectMembersPage, :using_params => {:project_name => project.name} do |page|
          page.add_member(user2)
        end
      end
      it ': Then user is added as a member to the project' do
        @current_page.refresh
        expect(@current_page.has_row_with_a_text?(user2.full_name)).to be true
      end
    end
  end
end

describe '[Project: Close]', :project do
  context 'When I have a project created by me' do
    user = RedmineUser.new
    project = RedmineProject.new
    include_context 'Register a new user', user
    include_context 'Create a new project', project
    context 'When I close project' do
      before :all do
        visit ProjectOverviewPage, :using_params => {:project_name => project.name} do |page|
          page.close_project
        end
      end
      it ': Then project is closed' do
        expect(on(ProjectOverviewPage).project_is_closed_warning_element).to be_visible
      end
    end
  end
end