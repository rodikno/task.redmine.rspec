require './spec/spec_helper'

describe '[User: Registration]', :user do
  context 'When I register a new user' do
    user = RedmineUser.new
    include_examples 'Successfully registered user', user
  end
end

describe '[User: Log in]', :user do
  context 'When I have an existing valid user' do
    user = RedmineUser.new
    include_context 'Register a new user', user
    context 'When I log in' do
      include_examples 'Successfully log in as', user
    end
  end
end

describe '[User: Change password]', :user do
  context 'When I log in as an existing user' do
    user = RedmineUser.new
    include_context 'Register a new user', user
    include_context 'Log in as', user
    context 'When I change password' do
      let(:new_password) { Faker::Internet.password(4) }
      before do
        visit(ChangePasswordPage).change_password(user, new_password)
      end
      it ': Then password is changed' do
        expect(on(ChangePasswordPage).success_message_element).to be_visible
      end
    end
  end
end
