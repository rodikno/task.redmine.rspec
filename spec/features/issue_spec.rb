require './spec/spec_helper'

describe '[Issue: Create]', :issue do
  context 'When I have an existing project' do
    user = RedmineUser.new
    project = RedmineProject.new
    issue = RedmineIssue.new
    include_context 'Register a new user', user
    include_context 'Create a new project', project
    context 'When I create an issue' do
      include_context 'Create issue in a project', issue, project
      it ': Then issue is created' do
        expect(on(IssueDetailsPage).success_message_element).to be_visible
      end
    end
  end
end

describe '[Issue: Assign user]', :issue do
  context 'When I have two existing users' do
    user1 = RedmineUser.new
    user2 = RedmineUser.new
    include_context 'Register users', user1, user2
    context 'When I have a project with a second user added as a member' do
      project = RedmineProject.new
      include_context 'Log in as', user1
      include_context 'Create a new project', project
      include_context 'Add user as a member to project', user2, project
      context 'When I create an issue and assign a second user to it' do
        issue = RedmineIssue.new
        include_context 'Create issue in a project', issue, project
        include_context 'Assign user to an issue', user2, issue
        it ': Then user is assigned to an issue' do
          expect(on(IssueDetailsPage).get_assigned_user_full_name).to eql(user2.full_name)
        end
      end
    end
  end
end

describe '[Issue: Log time]', :issue do
  context 'When I have an issue assigned to me by another user' do
    user1 = RedmineUser.new
    user2 = RedmineUser.new
    project = RedmineProject.new
    issue = RedmineIssue.new
    include_context 'User has a project with an issue assigned to another user', user1, project, issue, user2
    include_context 'Log in as', user2
    context 'When I log random time spent on issue' do
      time = rand(10)
      include_context 'Log time for issue', time, issue
      it ': Then corresponding time is logged for the issue' do
        expect(on(IssueDetailsPage).get_spent_time).to eql time
      end
    end
  end
end

describe '[Issue: Close]', :issue do
  context 'When I have an issue which I had previously logged time on' do
    user = RedmineUser.new
    project = RedmineProject.new
    issue = RedmineIssue.new
    time = rand(10)
    include_context 'Register a new user', user
    include_context 'Create a new project', project
    include_context 'Create issue in a project', issue, project
    include_context 'Assign user to an issue', user, issue
    include_context 'Log time for issue', time, issue
    context 'When I close issue' do
      include_context 'Close issue', issue
      it ': Then issue is closed' do
        expect(on(IssueDetailsPage).is_closed?).to be true
      end
    end
  end
end