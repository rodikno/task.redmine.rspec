require './spec/spec_helper'


describe '[Full flow]', :flow do
  before :all do
    @first_user = RedmineUser.new
    @second_user = RedmineUser.new
    @project = RedmineProject.new
    @issue = RedmineIssue.new
  end

  describe '[User: registration]' do
    context 'When I register a first user' do
      before :all do
        visit(RegistrationPage).register_user(@first_user)
      end
      it ': Then user is registered' do
        expect(on(MyAccountPage).success_message_element).to be_visible
      end
    end
    context 'When I register the second user' do
      before :all do
        visit(RegistrationPage).register_user(@second_user)
      end
      it ': Then user is registered' do
        expect(on(MyAccountPage).success_message_element).to be_visible
      end
    end
  end

  describe '[User: log in]' do
    context 'When I log in as a first user' do
      before :all do
        log_out
        log_in_as(@first_user)
      end
      it ': Then user is logged in' do
        expect(on(BasicPage).get_current_user_id).to eql @first_user.id
      end
    end
  end

  describe '[User: change password]' do
    context 'When I log in as a first user' do
      before :all do
        log_out
        log_in_as(@first_user)
      end
      context 'When I change password' do
        let(:new_password) { Faker::Internet.password(4) }
        before do
          visit(ChangePasswordPage).change_password(@first_user, new_password)
        end
        it ': Then password is changed' do
          expect(on(ChangePasswordPage).success_message_element).to be_visible
        end
      end
    end
  end

  describe '[Project: create]' do
    context 'When I log in as a first user' do
      before :all do
        log_out
        log_in_as(@first_user)
      end
      context 'When I create a new project' do
        before :all do
          visit(CreateProjectPage).create_project(@project)
        end
        it ': Then success message is shown' do
          on ProjectSettingsPage do |page|
            expect(page).to have_success_message
          end
        end
        it ': Then correct project title is shown' do
          on ProjectSettingsPage do |page|
            expect(page.project_title).to eql @project.name
          end
        end
      end
    end
  end

  describe '[Project: add member]' do
    context 'When I log in as a first user' do
      before :all do
        log_out
        log_in_as(@first_user)
      end
      context 'When I add the second user as a member to my project' do
        before do
          visit ProjectMembersPage, :using_params => {:project_name => @project.name} do |page|
            page.add_member(@second_user)
          end
        end
        it ': Then second user is added as a project member' do
          @current_page.refresh
          expect(@current_page.has_row_with_a_text?(@second_user.full_name)).to be true
        end
      end
    end
  end

  describe '[Project: create issue]' do
    context 'When I log in as a first user' do
      before :all do
        log_out
        log_in_as(@first_user)
      end
      context 'When I create an issue as a first user' do
        before :all do
          visit CreateIssuePage, :using_params => {:project_name => @project.name} do |page|
            page.create_issue(@issue)
            @issue.id = on(IssueDetailsPage).get_current_issue_id
          end
        end
        it ': Then issue is created' do
          expect(on(IssueDetailsPage).success_message_element).to be_visible
        end
      end
    end
  end

  describe '[Issue: assign user]' do
    context 'When I log in as a first user' do
      before :all do
        log_out
        log_in_as(@first_user)
      end
      context 'When I assign second user to the previously created issue' do
        before :all do
          visit(IssueDetailsPage, :using_params => {:issue_id => @issue.id}).assign_user(@second_user)
          @issue.assigned_user = @second_user
        end
        it ': Then second user is assigned to the issue' do
          expect(on(IssueDetailsPage).get_assigned_user_full_name).to eql(@second_user.full_name)
        end
      end
    end
  end

  describe '[Issue: log time]' do
    context 'When I login as a second user' do
      before :all do
        log_out
        log_in_as(@second_user)
      end
      context 'When I log random time spent on the issue assigned to me' do
        before :all do
          visit LogIssueTimePage, :using_params => {:issue_id => @issue.id} do |page|
            @spent_time = rand(10)
            page.log_time(@spent_time)
          end
          visit(IssueDetailsPage, :using_params => {:issue_id => @issue.id})
        end
        it ': Then I see corresponding time is logged for the issue' do
          expect(on(IssueDetailsPage).get_spent_time).to eql @spent_time
        end
      end
    end
  end

  describe '[Issue: close]' do
    context 'When I login as a second user' do
      before :all do
        log_out
        log_in_as(@second_user)
      end
      context 'When I close the issue which I had previously logged time on' do
        before :all do
          visit(IssueDetailsPage, :using_params => {:issue_id => @issue.id}).close_issue
        end
        it ': Then the issue is closed' do
          expect(on(IssueDetailsPage).is_closed?).to be true
        end
      end
    end
  end

  describe '[Project: close]' do
    context 'When I login as a second user' do
      before :all do
        log_out
        log_in_as(@second_user)
      end
      context 'When I close the project' do
        before :all do
          visit ProjectOverviewPage, :using_params => {:project_name => @project.name} do |page|
            page.close_project
          end
        end
        it ': Then project is closed' do
          expect(on(ProjectOverviewPage).project_is_closed_warning_element).to be_visible
        end
      end
    end
  end

end
